<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['cleanrequest']], function () {
    Route::get('/startdrink', 'SearchController@startdrink');
    Route::get('/enddrink', 'SearchController@enddrink');
    Route::get('/removeolddrinks', 'SearchController@removeolddrinks');
    Route::get('/getdrinkers', 'SearchController@getdrinkers');
});
