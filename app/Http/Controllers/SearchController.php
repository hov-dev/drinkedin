<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search;
use DB;

class SearchController extends Controller
{
    public function startdrink(Request $request, Search $search)
    {

        // если пользователь уже не стоит в поиске
        // то добавляем его в поиск
        if(is_null($request->status)) {
            $search->city = $request->city;
            $search->phone = $request->phone;
            $search->save();
        }

        $result['city'] = $request->city;
        $result['phone'] = $request->phone;
        $result['status'] = 1;
        return $result;

    }

    public function enddrink(Request $request, Search $search)
    {

        // если пользователь уже не стоит в поиске
        if(!is_null($request->phone)) {
            $search = $search
                ->where('phone', $request->phone)
                ->delete();
        }

        $result['phone'] = $request->phone;
        $result['status'] = 1;
        return $result;
    }

    public function removeolddrinks(Search $search)
    {

        // удаляем всех пользователей которые стоят в очереди 6 часов
        // вероятно они уже побухали и сейчас занимаются делами
        // чтобы им не спамили
        
        $result = DB::table('searches')
            ->whereRaw('created_at < now() - interval 180 minute')
            ->delete();

        $result = [];
        $result['status'] = 1;

        return $result;
    }


    public function getdrinkers(Request $request, Search $search)
    {

        $drinkers = $search
            ->where('city', $request->city)
            ->get();



        $result['status'] = 1;
        $result['drinkers'] = $drinkers;
        return $result;
    }
}
