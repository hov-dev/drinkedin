<?php

namespace App\Http\Middleware;

use Closure;

class ClearRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


         // clear requests for strip tags
         $request->phone = strip_tags($request->phone);
         $request->city = strip_tags($request->city);


        return $next($request);
    }
}
